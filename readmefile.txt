PetPeers Web Application Specifications

Front End Feature Requirements:  
------------------------------
1. Have to develop the Web Pages with HTML, CSS in JSP Pages. 

2. Have to develop JSP form pages with Spring Form tag library Features.

3. Have to implement 'User Autentication' with Servlet Filter.

4. Have to develop Unique Username feature while Registering as new user.

5. Have to Test Each URL End Point of Web Application.

Back  End Feature Requirements:  
-------------------------------
1. Have to Provide Proper Exception Handling Features.

2. Have to use Log4j2 for Debugging at necessary stages in the code building.

3. Have to implement Spring MVC Feature.

4. Have to develop code in necessary layered-structure.

5. Have to perform Validation with Spring 'Validator' interface Feature.

6. Have to provide necessary error messages with "i18n" Feature.

7. Have to use Hibernate (ORM Framework) efficently.

8. Have to create Database, Tables and inserting data with applicable constraints.

9. Have to write sufficient Unit Testing Suite Cases.


Note: Have to follow the Coding Standards while developing Application.

Front End Stack Technologies:
----------------------------
HTML, CSS, JSP, Spring MVC

Back  End Stack Technologies:
----------------------------
Java 8, Servlets, Spring Framework, Hibernate (ORM Framework with JPA), SQL


Tools and Frameworks Used: 
-------------------------
Git (bitbucket) (Distributed Version Control System) (2.29.2)
Open JDK 8 (1.8) [JSE + JEE]
Apache Maven (Build Tool) (3.8.1)
Apache Log4j2
Eclipse IDE (Oxygen.3a)
Apache Tomat Web Server (9.0)
MySQL Client (Database) (8.0) 
JUnit 4 (Unit Testing == TTD) 
Spring Framework (5.2.12) (opensource / freeware)
Hibernate (5.3) (opensource / freeware) (ORM Tool with JPA) 
Servlets & JSP == J2EE Web Components


Version Compatability Check:
---------------------------
1. Spring Framework 5.1 requires JDK 8 and officially supports Java 11. (so minimum supported version is JDK 8)
2. Hibernate version 5.3 Compatible with Java 8 and the latest version, JPA 2.1 and Hibernate ORM 5.2



Coding Standards and Best Practices included at workflow:
--------------------------------------------------------
1. Following the Naming Conventions for 
	- classes, interfaces, methods/behaviour, variables/properties.
2. Writing the clean and maintanable code == (as daily practice)
3. Formatting the code base. == (ctrl + shift + f) and (ctrl + shift + s == save)
4. Writing clear/understandable Comments.
5. Logging the application at neccessary steps.
6. Using Interfaces for loose coupling.
7. Including the Unit Test Cases (TTD)
8. Refactoring the code at required stages in code development.